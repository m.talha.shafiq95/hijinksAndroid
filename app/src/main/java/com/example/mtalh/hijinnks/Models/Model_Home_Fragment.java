package com.example.mtalh.hijinnks.Models;

/**
 * Created by CP on 1/25/2018.
 */

public class Model_Home_Fragment {
    String user_name;
    int user_profil_Image;
    String intrest;
    String title_of_event;
    String video_url;

    public Model_Home_Fragment(String user_name, int user_profil_Image, String intrest, String title_of_event, String video_url) {

        this.user_name = user_name;
        this.user_profil_Image = user_profil_Image;
        this.intrest = intrest;
        this.title_of_event = title_of_event;
        this.video_url = video_url;
    }

    public String getUser_name() {
        return user_name;
    }

    public void setUser_name(String user_name) {
        this.user_name = user_name;
    }

    public int getUser_profil_Image() {
        return user_profil_Image;
    }

    public void setUser_profil_Image(int user_profil_Image) {
        this.user_profil_Image = user_profil_Image;
    }

    public String getIntrest() {
        return intrest;
    }

    public void setIntrest(String intrest) {
        this.intrest = intrest;
    }

    public String getTitle_of_event() {
        return title_of_event;
    }

    public void setTitle_of_event(String title_of_event) {
        this.title_of_event = title_of_event;
    }

    public String getVideo_url() {
        return video_url;
    }

    public void setVideo_url(String video_url) {
        this.video_url = video_url;
    }


}
