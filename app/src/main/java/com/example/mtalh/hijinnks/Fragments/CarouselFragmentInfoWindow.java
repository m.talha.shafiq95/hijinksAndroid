package com.example.mtalh.hijinnks.Fragments;

import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.Toast;

import com.example.mtalh.hijinnks.CustomeUI.MyLinearLayout;
import com.example.mtalh.hijinnks.R;

/**
 * Created by mtalh on 27-Jan-18.
 */

public class CarouselFragmentInfoWindow extends Fragment {
    public static Fragment newInstance(Map_Fragment context, int pos, float scale) {
        Bundle b = new Bundle();
        b.putInt("pos", pos);
        b.putFloat("scale", scale);
        return Fragment.instantiate(context.getContext(), CarouselFragmentInfoWindow.class.getName(), b);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        if (container == null) {
            return null;
        }
        LinearLayout l = (LinearLayout) inflater.inflate(R.layout.listrow_map_infowindow, container, false);

        int pos = this.getArguments().getInt("pos");
        ImageView imageView = (ImageView) l.findViewById(R.id.mapcover);
        imageView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Toast.makeText(getActivity(), "I am imageeeeeeeeeeeeee", Toast.LENGTH_SHORT).show();

            }
        });


        MyLinearLayout root = (MyLinearLayout) l.findViewById(R.id.map_info_window_linearlayout);
        float scale = this.getArguments().getFloat("scale");
        root.setScaleBoth(scale);

        return l;
    }
}

