package com.example.mtalh.hijinnks.RecyclerViewAdapter;

import android.content.Context;
import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.drawable.BitmapDrawable;
import android.media.MediaPlayer;
import android.media.ThumbnailUtils;
import android.net.Uri;
import android.os.AsyncTask;
import android.provider.MediaStore;
import android.support.v4.app.FragmentTransaction;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.MotionEvent;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.MediaController;
import android.widget.ProgressBar;
import android.widget.TextView;
import android.widget.VideoView;

import com.example.mtalh.hijinnks.Activites.Comments;
import com.example.mtalh.hijinnks.Fragments.Map_Fragment;
import com.example.mtalh.hijinnks.Fragments.User_Profile_Fragment;
import com.example.mtalh.hijinnks.Models.Model_Home_Fragment;
import com.example.mtalh.hijinnks.R;

import java.util.ArrayList;

import de.hdodenhof.circleimageview.CircleImageView;


/**
 * Created by mtalh on 24-Oct-17.
 */

public class RecyclerAdapter_Home_HomeFragment extends RecyclerView.Adapter<RecyclerAdapter_Home_HomeFragment.RecyclerViewHolder> {

    static ItemClickListener itemClickListener;
    Context context;
    boolean video_pause_play = false;
    ArrayList<Model_Home_Fragment> model_home_fragments = new ArrayList<>();
    private Map_Fragment mapFragment = new Map_Fragment();
    private User_Profile_Fragment user_profile_fragment = new User_Profile_Fragment();
    public RecyclerAdapter_Home_HomeFragment(ArrayList<Model_Home_Fragment> model_home_fragments, Context context) {
        this.model_home_fragments = model_home_fragments;
        this.context = context;
    }
    @Override
    public RecyclerViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.listrow_home_homefragment, parent, false);
        RecyclerAdapter_Home_HomeFragment.RecyclerViewHolder recyclerviewholder = new RecyclerAdapter_Home_HomeFragment.RecyclerViewHolder(view);
        return recyclerviewholder;

    }
    @Override
    public void onBindViewHolder(final RecyclerViewHolder holder, int position) {
        holder.user_Name.setText(model_home_fragments.get(position).getUser_name());
        holder.user_profile_image.setImageResource(model_home_fragments.get(position).getUser_profil_Image());
        holder.intrest_textview.setText(model_home_fragments.get(position).getIntrest());
        holder.title_of_event.setText(model_home_fragments.get(position).getTitle_of_event());
        holder.videoView.setVideoURI(Uri.parse(model_home_fragments.get(position).getVideo_url()));
        holder.videoView.setOnTouchListener(new View.OnTouchListener() {
            @Override
            public boolean onTouch(View view, MotionEvent motionEvent) {

                if (video_pause_play == false) {

                    holder.videoView.start();
                    holder.video_play_pause.setImageResource(R.drawable.video_play_play);
                    video_pause_play = true;
                    holder.video_background_play.setVisibility(View.INVISIBLE);
                    holder.video_play_pause.setVisibility(View.INVISIBLE);
                } else {
                    holder.videoView.pause();
                    holder.video_play_pause.setImageResource(R.drawable.video_play_play);
                    holder.video_background_play.setVisibility(View.VISIBLE);
                    holder.video_play_pause.setVisibility(View.VISIBLE);
                    video_pause_play = false;
                }
                return false;
            }
        });
        holder.open_map_fragment.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                AppCompatActivity activity = (AppCompatActivity) view.getContext();
                activity.getSupportFragmentManager().beginTransaction().replace(R.id.home_framelayout, mapFragment).addToBackStack(null).commit();

            }
        });
        holder.user_profile_image.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                AppCompatActivity activity = (AppCompatActivity) view.getContext();
                activity.getSupportFragmentManager().beginTransaction().replace(R.id.home_framelayout, user_profile_fragment).addToBackStack(null).commit();

            }
        });
        holder.comment_section_screen.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                AppCompatActivity activity = (AppCompatActivity) view.getContext();
                Intent comment_activity = new Intent(activity, Comments.class);
                activity.startActivity(comment_activity);
            }
        });
    }

    @Override
    public int getItemCount() {
        return model_home_fragments.size();
    }

    public void SsetClickListener(ItemClickListener listener) {
        this.itemClickListener = listener;
    }

    public interface ItemClickListener {
        public void OnItemClick(int Pos);
    }

    public static class RecyclerViewHolder extends RecyclerView.ViewHolder implements View.OnClickListener {
        TextView user_Name, intrest_textview, title_of_event;
        VideoView videoView;
        ImageView video_background_play, video_play_pause;
        LinearLayout open_map_fragment;
        ProgressBar progess_bar_video_main_class;
        CircleImageView user_profile_image;
        LinearLayout comment_section_screen;

        public RecyclerViewHolder(View view) {
            super(view);
            user_Name = (TextView) view.findViewById(R.id.profile_name);
            intrest_textview = (TextView) view.findViewById(R.id.intrest_textview);
            title_of_event = (TextView) view.findViewById(R.id.title_of_event);
            videoView = (VideoView) view.findViewById(R.id.videoview);
            video_background_play = (ImageView) view.findViewById(R.id.video_background_play);
            video_play_pause = (ImageView) view.findViewById(R.id.video_play);
            progess_bar_video_main_class = (ProgressBar) view.findViewById(R.id.progess_bar_video);
            open_map_fragment = (LinearLayout) view.findViewById(R.id.open_map_fragment);
            comment_section_screen = (LinearLayout) view.findViewById(R.id.comment_section_screen);
            user_profile_image = (CircleImageView) view.findViewById(R.id.user_profile_image);
            view.setOnClickListener(this);
        }
        @Override
        public void onClick(View view) {
            itemClickListener.OnItemClick(getAdapterPosition());
        }
    }
}